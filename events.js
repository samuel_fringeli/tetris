window.onkeydown = function(event) {
	if (game_running) {
		var piece = tetris_pieces[tetris_pieces.length - 1];

	    if (event.keyCode == 32) { // espace
	    	// pour faire changer la rotation de la pièce
	        piece.rotate();
	    }
	    if (event.keyCode == 39) { // flèche droite
	    	// pour déplacer la pièce vers la droite
    		piece.move(sd, 0);
    		current_time = Date.now();
	    }
	    if (event.keyCode == 37) { // flèche gauche
	    	// pour déplacer la pièce vers la gauche
	    	piece.move(-sd, 0);
	    	current_time = Date.now();
	    }
	    if (event.keyCode == 40) { // flèche bas
	    	// pour accélérer la pièce
	    	interval_1_running = false;
	    	interval_2_running = true;
	    }
	}
}

window.onkeyup = function(event) {
	if (event.keyCode == 40) { // flèche bas
		// pour décélérer la pièce
		interval_1_running = true;
		interval_2_running = false;
	}
}

$('.pause_button').click(pause);
$('.game_button').click(function() {
	window.location.reload()
});