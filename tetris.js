var game_running = true;
var score = -1;

// contextes canvas
var canvas = $('#tetris_canvas').get(0);
var canvas_left = $('.game_infos_left canvas').get(0);
var canvas_right = $('.game_infos_right canvas').get(0);

var ctx = {
	center: canvas.getContext('2d'),
	left: canvas_left.getContext('2d'),
	right: canvas_right.getContext('2d'),
};

// toutes les instances des pièces du tetris
var tetris_pieces = [];

// nombre de pixels avec lesquels se déplace une pièce
var px_moving = 5;

/* interval auquel descendent les briques
le premier interval est celui qui s'exécute normalement,
tandis que le deuxième est celui qui s'exécute lorsque la touche down est appuyée */
var interval1 = 50;
var interval2 = 10;
var interval_1_running = true;
var interval_2_running = false;
var wait_interval = 1000;
var wait_interval_running;

// dimensions jeu
var sd = 40; // square_dimentions
var squares_width_number = canvas.width/sd;
var squares_height_number = canvas.height/sd;

// rognage en haut
canvas.style.marginTop = sd*-2 + 'px';

// contient les infos sur toutes les squares ; on peut savoir si chaque carré est plein ou vide
var all_squares_infos = {};

// pour sauvegarder/restaurer
var elements_to_save = ['rotation', 'rect_coord', 'piece_height', 'piece_width', 'ox', 'oy'];

// largeur des bordures
var thickness = 0.15;

canvas.height = canvas.height + px_moving;

// ajustement de la hauteur des canvas gauche et droite
$('.game_infos_right canvas').attr('height', canvas.height);

var next_tetris_pieces = [];
var pieces_in_queue = [];
var number_of_pieces_in_queue = 3;

// définit les trois premières pièces
for (var i = 0; i < number_of_pieces_in_queue; i++) {
	pieces_in_queue[i] = Math.floor(Math.random()*7);
}

// Textes sur les bandes gauche et droite
add_text('left', 'Score', 30, 15);
add_text('right', 'Next', 30, 20);

// Espace pour ces textes (coordonnée Y)
var space_for_text = 70;

// Initialise le score à zéro
update_score();

// initialisation des infos sur les carrés remplis
empty_all_squares();

// lancement de la première pièce
tetris_pieces.push(new TetrisPiece());

// fonction qui s'exécute périodiquement pour déplacer les pièces et gérer les collisions
function main() {
	if (game_running) {
		show_pieces_in_queue();
		var piece = tetris_pieces[tetris_pieces.length - 1];

		if (piece.collide()) {
			// la pièce entre en collision avec une autre pièce plus basse
			if (!wait_interval_running) {
				piece.move(0, px_moving);
			}
			end_actions(piece);
		}
		else if (piece.oy + piece.piece_height <= canvas.height - px_moving) {
			// la pièce n'entre pas en collision
			if (wait_interval_running) {
				wait_interval_running = false;
			}
			piece.move(0, px_moving);
		} else {
			// la pièce entre collision avec le bas du jeu
			end_actions(piece);
		}
	}
}

// jeu lent
window.setInterval(function() { if (interval_1_running && !wait_interval_running) { main(); }}, interval1);
// jeu plus rapide
window.setInterval(function() { if (interval_2_running && !wait_interval_running) { main(); }}, interval2);
// interval d'attente lorsqu'une pièce est arrivée en bas
window.setInterval(function() { if (wait_interval_running) { main(); }}, wait_interval);