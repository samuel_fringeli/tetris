function add_text(context, text, font_weight=30, ox=10, oy=40) {
	ctx[context].font = font_weight + 'px Arial';
	ctx[context].fillStyle = 'white';
	ctx[context].fillText(text, ox, oy);
}

function update_score() {
	score++;
	ctx.left.clearRect(0, space_for_text, canvas_left.width, canvas_left.height);
	var ox = 0;
	if (score < 1000) {
		var ox = 15;
	} if (score < 100) {
		var ox = 28;
	} if (score < 10) {
		var ox = 40;
	}
	add_text('left', score, 40, ox, 100);
}

function pause() {
	game_running = !game_running;
	$('.pause_button').text(($('.pause_button').text() == 'Pause') ? 'Resume':'Pause');
}

function new_piece_in_queue() {
	// supprime le premier élément
	pieces_in_queue.splice(0, 1);
	// ajoute une nouvelle pièce
	pieces_in_queue[pieces_in_queue.length] = Math.floor(Math.random()*7);
}

function empty_all_squares() {
	for (var i = 0; i < squares_width_number; i++) {
		square_x = i * sd;
		for (var j = 0; j < squares_height_number; j++) {
			square_y = j * sd;
			all_squares_infos['x_'+square_x+'_y_'+square_y] = 'empty';
		}
	}
}

function fill_squares_infos() {
	for (var i = 0; i < tetris_pieces.length; i++) {
		for (var j = 0; j < tetris_pieces[i].rect_coord.length; j++) {
			var top_left_y = tetris_pieces[i].rect_coord[j].top_left_y - px_moving;
			var top_left_x = tetris_pieces[i].rect_coord[j].top_left_x;
			all_squares_infos['x_'+top_left_x+'_y_'+top_left_y] = 'full';
		}
	}
}

function update_squares_infos(piece) {
	var coord = piece.rect_coord;

	for (var i = 0; i < coord.length; i++) {
		var top_left_x = coord[i].top_left_x;
		var top_left_y = coord[i].top_left_y - px_moving;

		if (typeof(all_squares_infos['x_'+top_left_x+'_y_'+top_left_y]) !== 'undefined') {
			all_squares_infos['x_'+top_left_x+'_y_'+top_left_y] = 'full';
		}
	}
}

function remove_row(y) { // supprime une ligne si elle est pleine
	var row_full = true;

	for (var x = 0; x < canvas.width; x = x + sd) {
		if (all_squares_infos['x_'+x+'_y_'+y] === 'empty') {
			row_full = false;
			break;
		}
	}
	if (row_full) {
		var to_draw = [];
		empty_all_squares();

		for (var i = 0; i < tetris_pieces.length; i++) {
			for (var j = 0; j < tetris_pieces[i].rect_coord.length; j++) {
				var top_left_y = tetris_pieces[i].rect_coord[j].top_left_y - px_moving;
				var top_left_x = tetris_pieces[i].rect_coord[j].top_left_x;
				if (top_left_y < y) {
					to_draw.push({
						top_left_y: top_left_y + sd,
						top_left_x: tetris_pieces[i].rect_coord[j].top_left_x,
						color: tetris_pieces[i].color,
					});
					tetris_pieces[i].rect_coord[j].top_left_y += sd;
				} else if (top_left_y == y) {
					// on met une grande coordonnée y aux carrés qui ont été supprimés
					tetris_pieces[i].rect_coord[j].top_left_y = canvas.height * 20;
				}
			}
		}

		ctx.center.clearRect(0, 0, canvas.width, y + sd + px_moving);

		for (var k = 0; k < to_draw.length; k++) {
			ctx.center.fillStyle = to_draw[k].color;
			fill_rect_with_borders(to_draw[k].top_left_x, to_draw[k].top_left_y + px_moving, sd, sd);
		}
		fill_squares_infos();
	}
}

function remove_all_full_rows() {
	for (var i = canvas.height - px_moving - sd; i >= 0; i -= sd) {
		for (var j = 0; j < 4; j++) {
			remove_row(i);
		}
	}
}

function fill_rect_with_borders(p1, p2, p3, p4, context='center') {
	var color = ctx[context].fillStyle;
	ctx[context].fillStyle = 'white';
	ctx[context].fillRect(p1, p2, p3, p4);
	ctx[context].fillStyle = color;
	ctx[context].fillRect(p1 + thickness, p2 + thickness, p3 - (thickness * 2), p4 - (thickness * 2));
}

function show_pieces_in_queue() {
	ctx.right.clearRect(0, space_for_text, canvas_right.width, canvas_right.height);
	for (var i = 0; i < pieces_in_queue.length; i++) {
		next_tetris_pieces.push(new TetrisPiece('right', pieces_in_queue[i], 10, (sd*5*i)+space_for_text));
	}
}

function end_actions(piece) {
	if (wait_interval_running) {
		piece.status = 'fixed';
		update_squares_infos(piece);
		remove_all_full_rows();
		update_score();
		if (game_running) {
			tetris_pieces.push(new TetrisPiece());
		}
		wait_interval_running = false;
	} else {
		wait_interval_running = true;
	}
}