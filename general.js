function ajust_play_button() {
	$('.logo img').each(function() {
		var img_width = $(this).width();
		$('.logo-text').css('width', (img_width/3)+'px');
		$('.logo-text').css('bottom', (img_width/3.5)+'px');
	});
}

$(window).resize(function() {
	ajust_play_button();
});

ajust_play_button();