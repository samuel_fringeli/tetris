function TetrisPiece(context='center', piece_number=0, ox=0, oy=0) {
	this.rotation = 0;
	if (context != 'center') {
		this.piece_number = piece_number;
		this.status = 'fixed';
		this.ox = ox; // origin_x
		this.oy = oy; // origin_y
	} else {
		this.piece_number = pieces_in_queue[0];
		new_piece_in_queue();
		this.status = 'moving';
		this.ox = Math.floor(Math.random()*(squares_width_number-1))*sd; // origin_x
		this.oy = 0; // origin_y
	}
	this.context = context;

	this.manage();
}

TetrisPiece.prototype.change_color = function() {
	switch(this.piece_number) {
		case 0: this.color = '#3F9BFC'; break;
		case 1: this.color = '#FCAC21'; break;
		case 2: this.color = '#855ABF'; break;
		case 3: this.color = '#EB5510'; break;
		case 4: this.color = '#345CB4'; break;
		case 5: this.color = '#14AD29'; break;
		case 6: this.color = '#DC2126'; break;
	}
	ctx[this.context].fillStyle = this.color;
}

TetrisPiece.prototype.fill_rect = function(w, h) {
	if (!this.only_get_piece_infos) {
		if (this.reset_mode) {
			ctx[this.context].clearRect(this.ox + w, this.oy + h, sd, sd, this.context);
		} else {
			fill_rect_with_borders(this.ox + w, this.oy + h, sd, sd, this.context);
		}
	}
	this.rect_coord.push({
		top_left_x: this.ox + w,
		top_left_y: this.oy + h,
		bottom_left_x: this.ox + w,
		bottom_left_y: this.oy + h + sd,
	});
}

TetrisPiece.prototype.build_piece = function(rotation, coords, piece_height, piece_width) {
	/* rect[0] = [width, height]; rect[1] = [width, height]; ... */
	for (var h = 0; h < rotation.length; h++) {
		if (this.rotation == rotation[h]) {
			this.piece_height = sd*piece_height;
			this.piece_width = sd*piece_width;

			for (var i = 0; i < coords.length; i++) {
				this.fill_rect(sd*coords[i][0], sd*coords[i][1]);
			}
		}	
	}
}

TetrisPiece.prototype.manage = function(reset_mode = false, only_get_piece_infos = false) {
	this.rect_coord = [];
	this.reset_mode = reset_mode;
	this.only_get_piece_infos = only_get_piece_infos;
	this.change_color();

	switch(this.piece_number) {
		case 0:
			this.build_piece([0, 180], [[0, 0], [0, 1], [0, 2], [0, 3]], 4, 1);
			this.build_piece([90, 270], [[0, 0], [1, 0], [2, 0], [3, 0]], 1, 4);
			return;
		case 1:
			this.build_piece([0, 90, 180, 270], [[0, 0], [0, 1], [1, 1], [1, 0]], 2, 2);
			return;
		case 2:
			this.build_piece([0], [[0, 0], [0, 1], [0, 2], [1, 1]], 3, 2);
			this.build_piece([90], [[0, 0], [1, 0], [2, 0], [1, 1]], 2, 3);
			this.build_piece([180], [[0, 1], [1, 0], [1, 1], [1, 2]], 3, 2);
			this.build_piece([270], [[0, 1], [1, 0], [1, 1], [2, 1]], 2, 3);
			return;
		case 3:
			this.build_piece([0], [[0, 0], [0, 1], [0, 2], [1, 0]], 3, 2);
			this.build_piece([90], [[0, 0], [1, 0], [2, 0], [2, 1]], 2, 3);
			this.build_piece([180], [[1, 0], [1, 1], [1, 2], [0, 2]], 3, 2);
			this.build_piece([270], [[0, 0], [0, 1], [1, 1], [2, 1]], 2, 3);
			return;
		case 4:
			this.build_piece([0], [[0, 0], [1, 0], [1, 1], [1, 2]], 3, 2);
			this.build_piece([90], [[0, 1], [1, 1], [2, 1], [2, 0]], 2, 3);
			this.build_piece([180], [[0, 0], [0, 1], [0, 2], [1, 2]], 3, 2);
			this.build_piece([270], [[0, 0], [1, 0], [2, 0], [0, 1]], 2, 3);
			return;
		case 5:
			this.build_piece([0, 180], [[0, 0], [0, 1], [1, 1], [1, 2]], 3, 2);
			this.build_piece([90, 270], [[0, 1], [1, 1], [1, 0], [2, 0]], 2, 3);
			return;
		case 6:
			this.build_piece([0, 180], [[1, 0], [1, 1], [0, 1], [0, 2]], 3, 2);
			this.build_piece([90, 270], [[0, 0], [1, 0], [1, 1], [2, 1]], 2, 3);
			return;
	}
}

TetrisPiece.prototype.save = function() {
	for (var i = 0; i < elements_to_save.length; i++) {
		this['saved_' + elements_to_save[i]] = this[elements_to_save[i]];
	}
}

TetrisPiece.prototype.restore = function() {
	for (var i = 0; i < elements_to_save.length; i++) {
		this[elements_to_save[i]] = this['saved_' + elements_to_save[i]];
	}
}

TetrisPiece.prototype.move = function(move_x, move_y) {
	// réinitialise l'ancienne pièce du tetris
	this.manage(reset_mode = true);

	this.save();
	this.ox = this.ox + move_x;
	this.oy = this.oy + move_y;

	if (move_x != 0) {
		// simuler le déplacement
		this.manage(reset_mode = false, only_get_piece_infos = true);

		if (this.collide()) {
			this.restore();
		}
	}
		
	this.manage();
}

TetrisPiece.prototype.rotate = function() {
	// réinitialise l'ancienne pièce du tetris
	this.manage(reset_mode = true);

	this.save();
	this.rotation = (this.rotation + 90) % 360;
	// simuler la rotation
	this.manage(reset_mode = false, only_get_piece_infos = true);

	// simuler la collision avec une autre pièce
	if (this.collide()) {
		// vérifier si la pièce dépasse le cadre du jeu à droite
		if (this.oy + this.piece_width >= canvas.width) {
			// alert('la pièce dépasse le cadre du jeu à droite si on essaie de la faire tourner');
		}
		this.restore();
	}
	// simuler la collision avec le bas du jeu
	else if (this.oy + this.piece_height >= canvas.height - px_moving) {
		this.restore();
	}

	this.manage();
}

TetrisPiece.prototype.collide = function() {
	var coord = this.rect_coord;
	var result = false;

	for (var i = 0; i < coord.length; i++) {
		var bottom_left_x = coord[i].bottom_left_x;
		var bottom_left_y = coord[i].bottom_left_y;

		if (bottom_left_x < 0 || bottom_left_x + sd > canvas.width) {
			result = true;
			var collide_left_right = true;
		}
		var in_loop = false;
		var infinite_loop = false;

		while (typeof(all_squares_infos['x_'+bottom_left_x+'_y_'+bottom_left_y]) === 'undefined') {
			in_loop = true;
			bottom_left_y = bottom_left_y - px_moving;

			// pour éviter une boucle infinie
			if (bottom_left_y < 0) {
				infinite_loop = true;
				break;
			}
		}
		if (!infinite_loop) {
			if (all_squares_infos['x_'+bottom_left_x+'_y_'+bottom_left_y] === 'full') {
				result = true;
			} 
			else if (in_loop && all_squares_infos['x_'+bottom_left_x+'_y_'+(bottom_left_y - sd)] === 'full') {
				result = true;
			}
		}
	}
	if (result && this.oy < sd*3 && !collide_left_right) { // lorsque les pièces ont une collision en haut
		game_running = false;
	}
	return result;
}